/* 
 * File:   Scanner.cpp
 * Author: blacke
 * 
 * Created on April 11, 2013, 11:29 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include "Scanner.h"

#define TIPONODO_INTERMEDIO      (0)
#define TIPONODO_INTERMEDIO_FINAL (1)
#define TIPONODO_FINAL           (2)


#define SCANNER_ERROR (-1)
#define SCANNER_TOKEN_RECONOCIDO (-1)
#define AUTOMATA_ESTADOS_NUM (46)                       //total de estados
#define AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL (85)        //cadenas esperadas sin contar los ultimos
                                                        //estados finales que son
                                                        // \n , " ", EOF
#define AUTOMATA_TIPO_NODO (AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL - 2)    //tipo de nodo
#define AUTOMATA_CODIGO_SALIDA (AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL - 1)	//terminal o no

#define SIMB_T_programa (1)
#define SIMB_T_LLAVE_ABRE (2)
#define SIMB_T_LLAVE_CIERRA (3)
#define SIMB_T_PUNTO_Y_COMA (4)
#define SIMB_T_IGUAL (5)
#define SIMB_T_DOBLE_IGUAL (6)
#define SIMB_T_if (7)
#define SIMB_T_PARENTESIS_ABRE (8)
#define SIMB_T_PARENTESIS_CIERRA (9)
#define SIMB_T_else (10)
#define SIMB_T_INTERROGACION (11)
#define SIMB_T_ADMIRACION (12)
#define SIMB_T_DIFERENTE (13)
#define SIMB_T_SUMA (14)
#define SIMB_T_RESTA (15)
#define SIMB_T_MULTIPLICACION (16)
#define SIMB_T_DIVISION (17)
#define SIMB_T_MENOR (18)
#define SIMB_T_MAYOR (19)
#define SIMB_T_MENOR_IGUAL (20)
#define SIMB_T_MAYOR_IGUAL (21)
#define SIMB_T_POTENCIA (22)
#define SIMB_T_DIGITO_ENTERO (23)
#define SIMB_T_DIGITO_DECIMAL (24)
#define SIMB_T_GUION_BAJO (25)
#define SIMB_T_while (26)
#define SIMB_T_ID (27)
#define SIMB_T_ESP (28)
#define SIMB_T_TAB (29)
#define SIMB_T_SALTO_DE_LINEA (30)
#define SIMB_T_EOF (31)




int MT[AUTOMATA_ESTADOS_NUM][AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL] = 
{
    
{45,45,45,45,11,45,45,45,9,45,45,45,45,45,45,1,45,45,45,45,45,45,15,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,43,43,43,43,43,43,43,43,43,45,20,21,22,24,25,26,27,28,29,30,31,33,35,37,38,-1,39,40,41,42,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,2,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,3,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,4,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,5,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{6,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,7,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{8,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_programa},
{45,45,45,45,45,10,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_if},
{45,45,45,45,45,45,45,45,45,45,45,12,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,13,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,14,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_else},
{45,45,45,45,45,45,45,16,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,17,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,18,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,19,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO,SCANNER_ERROR},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_while},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_LLAVE_ABRE},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_LLAVE_CIERRA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,23,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_DOBLE_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_PARENTESIS_ABRE},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_PARENTESIS_CIERRA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_SUMA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_RESTA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_MULTIPLICACION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_DIVISION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_POTENCIA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,32,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_ADMIRACION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_DIFERENTE},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,34,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_MENOR},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_MENOR_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,36,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_MAYOR},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_MAYOR_IGUAL},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_PUNTO_Y_COMA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_INTERROGACION},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_ESP},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_TAB},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_SALTO_DE_LINEA},
{-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_FINAL,SIMB_T_EOF},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,43,43,43,43,43,43,43,43,43,43,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,44,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_DIGITO_ENTERO},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,44,44,44,44,44,44,44,44,44,44,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_DIGITO_DECIMAL},
{45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,TIPONODO_INTERMEDIO_FINAL,SIMB_T_ID},

    
    
    
};


Scanner::Scanner() {
}

Scanner::Scanner(const Scanner& orig) {
}

Scanner::~Scanner() {
}


int Scanner::ObtenerCodigoSimboloEntrada(int intASCIISimboloEntrada)
{    
    int intCodigoSimboloEntrada = -1;   
    switch(intASCIISimboloEntrada)
    {
        case 'a' : intCodigoSimboloEntrada = 0; break;
        case 'b' : intCodigoSimboloEntrada = 1; break;
        case 'c' : intCodigoSimboloEntrada = 2; break;
        case 'd' : intCodigoSimboloEntrada = 3; break;
        case 'e' : intCodigoSimboloEntrada = 4; break;
        case 'f' : intCodigoSimboloEntrada = 5; break;
        case 'g' : intCodigoSimboloEntrada = 6; break;
        case 'h' : intCodigoSimboloEntrada = 7; break;
        case 'i' : intCodigoSimboloEntrada = 8; break;
        case 'j' : intCodigoSimboloEntrada = 9; break;
        case 'k' : intCodigoSimboloEntrada = 10; break;
        case 'l' : intCodigoSimboloEntrada = 11; break;
        case 'm' : intCodigoSimboloEntrada = 12; break;
        case 'n' : intCodigoSimboloEntrada = 13; break;
        case 'o' : intCodigoSimboloEntrada = 14; break;
        case 'p' : intCodigoSimboloEntrada = 15; break;
        case 'q' : intCodigoSimboloEntrada = 16; break;
        case 'r' : intCodigoSimboloEntrada = 17; break;
        case 's' : intCodigoSimboloEntrada = 18; break;
        case 't' : intCodigoSimboloEntrada = 19; break;
        case 'u' : intCodigoSimboloEntrada = 20; break;
        case 'v' : intCodigoSimboloEntrada = 21; break;
        case 'w' : intCodigoSimboloEntrada = 22; break;
        case 'x' : intCodigoSimboloEntrada = 23; break;
        case 'y' : intCodigoSimboloEntrada = 24; break;
        case 'z' : intCodigoSimboloEntrada = 25; break;
        case 'A' : intCodigoSimboloEntrada = 26; break;
        case 'B' : intCodigoSimboloEntrada = 27; break;
        case 'C' : intCodigoSimboloEntrada = 28; break;
        case 'D' : intCodigoSimboloEntrada = 29; break;
        case 'E' : intCodigoSimboloEntrada = 30; break;
        case 'F' : intCodigoSimboloEntrada = 31; break;
        case 'G' : intCodigoSimboloEntrada = 32; break;
        case 'H' : intCodigoSimboloEntrada = 33; break;
        case 'I' : intCodigoSimboloEntrada = 34; break;
        case 'J' : intCodigoSimboloEntrada = 35; break;
        case 'K' : intCodigoSimboloEntrada = 36; break;
        case 'L' : intCodigoSimboloEntrada = 37; break;
        case 'M' : intCodigoSimboloEntrada = 38; break;
        case 'N' : intCodigoSimboloEntrada = 39; break;
        case 'O' : intCodigoSimboloEntrada = 40; break;
        case 'P' : intCodigoSimboloEntrada = 41; break;
        case 'Q' : intCodigoSimboloEntrada = 42; break;
        case 'R' : intCodigoSimboloEntrada = 43; break;
        case 'S' : intCodigoSimboloEntrada = 44; break;
        case 'T' : intCodigoSimboloEntrada = 45; break;
        case 'U' : intCodigoSimboloEntrada = 46; break;
        case 'V' : intCodigoSimboloEntrada = 47; break;
        case 'W' : intCodigoSimboloEntrada = 48; break;
        case 'X' : intCodigoSimboloEntrada = 49; break;
        case 'Y' : intCodigoSimboloEntrada = 50; break;
        case 'Z' : intCodigoSimboloEntrada = 51; break;
        case '0' : intCodigoSimboloEntrada = 52; break;
        case '1' : intCodigoSimboloEntrada = 53; break;
        case '2' : intCodigoSimboloEntrada = 54; break;
        case '3' : intCodigoSimboloEntrada = 55; break;
        case '4' : intCodigoSimboloEntrada = 56; break;
        case '5' : intCodigoSimboloEntrada = 57; break;
        case '6' : intCodigoSimboloEntrada = 58; break;
        case '7' : intCodigoSimboloEntrada = 59; break;
        case '8' : intCodigoSimboloEntrada = 60; break;
        case '9' : intCodigoSimboloEntrada = 61; break;
        case '_' : intCodigoSimboloEntrada = 62; break;
        case '{' : intCodigoSimboloEntrada = 63; break;
        case '}' : intCodigoSimboloEntrada = 64; break;
        case '=' : intCodigoSimboloEntrada = 65; break;
        case '(' : intCodigoSimboloEntrada = 66; break;
        case ')' : intCodigoSimboloEntrada = 67; break;
        case '+' : intCodigoSimboloEntrada = 68; break;
        case '-' : intCodigoSimboloEntrada = 69; break;
        case '*' : intCodigoSimboloEntrada = 70; break;
        case '/' : intCodigoSimboloEntrada = 71; break;
        case '^' : intCodigoSimboloEntrada = 72; break;
        case '!' : intCodigoSimboloEntrada = 73; break;
        case '<' : intCodigoSimboloEntrada = 74; break;
        case '>' : intCodigoSimboloEntrada = 75; break;
        case ';' : intCodigoSimboloEntrada = 76; break;
        case '?' : intCodigoSimboloEntrada = 77; break;
        case '.' : intCodigoSimboloEntrada = 78; break;
        case ' ' : intCodigoSimboloEntrada = 79; break;         //ESP
        case '\t'  : intCodigoSimboloEntrada = 80; break;
        case '\n'  : intCodigoSimboloEntrada = 81; break;
        case -1  : intCodigoSimboloEntrada = 82; break;		//EOF
        default: break;
    }//switch

    return intCodigoSimboloEntrada;
}//ObtenerCodigoSimboloEntrada

string Scanner::ObtenerEtiquetaDelSimboloTerminal(int intCodigoSimboloTerminal)
{
    string strRes;

    switch(intCodigoSimboloTerminal)
    {
        case 1 : strRes = "PROGRAMA"; break;
        case 2 : strRes = "LLAVE_ABRE"; break;
        case 3 : strRes = "LLAVE_CIERRA"; break;
        case 4 : strRes = "PUNTO_Y_COMA"; break;
        case 5 : strRes = "IGUAL"; break;
        case 6 : strRes = "DOBLE_IGUAL"; break;
        case 7 : strRes = "if"; break;
        case 8 : strRes = "PARENTESIS_ABRE"; break;
        case 9 : strRes = "PARENTESIS_CIERRA"; break;
        case 10 : strRes = "else"; break;
        case 11 : strRes = "INTERROGACION"; break;
        case 12 : strRes = "ADMIRACION"; break;
        case 13 : strRes = "DIFERENTE"; break;
        case 14 : strRes = "SUMA"; break;
        case 15 : strRes = "RESTA"; break;
        case 16 : strRes = "MULTIPLICACION"; break;
        case 17 : strRes = "DIVISION"; break;
        case 18 : strRes = "MENOR"; break;
        case 19 : strRes = "MAYOR"; break;
        case 20 : strRes = "MENOR_IGUAL"; break;
        case 21 : strRes = "MAYOR_IGUAL"; break;
        case 22 : strRes = "POTENCIA"; break;
        case 23 : strRes = "DIGITO_ENTERO"; break;
        case 24 : strRes = "DIGITO_DECIMAL"; break;
        case 25 : strRes = "GUION_BAJO"; break;
        case 26 : strRes = "while"; break;
        case 27 : strRes = "IDENTIFICADOR"; break;
        case 28 : strRes = "ESP"; break;
        case 29 : strRes = "TAB"; break;
        case 30 : strRes = "SALTO_DE_LINEA"; break;
        case 31 : strRes = "EOF"; break;
    }//switch
    return strRes;
}//ObtenerEtiquetaDelSimboloTerminal

void Scanner::LeerArchivo(string path)
{
   
}

int Scanner::escanear(string strArchivo)
{
    FILE *ptrArchivoFuente;             //TIPO FILE POR EL getc de c/caracter
    int intCodigoSimboloEntrada;
    int intCodigoSimboloTerminal;
    int intEstadoSiguiente;
    int intTipoNodo;
    bool bolLeerSigCar = true;
    string strTokenSimboloTerminal;

//    for (int x = 0; x<AUTOMATA_ESTADOS_NUM; x++)
//        for(int y=0; y < AUTOMATA_SIMBOLOS_ENTRADA_Y_CONTROL;y++)
//            cout << "MT = [" << x << "]["<< y<<"] = " << MT[x][y]<<endl;
    ptrArchivoFuente = fopen(strArchivo.c_str(), "r");

    if (ptrArchivoFuente==NULL)
        cout<<"ERROR EN UN ARCHIVO FUENTE"<<endl;
    else
    {
        //s := s0;
        int intEstadoActual = 0;
        //c := siguientecaracter;
        char c;
//	char previa_c;
        //while c <> eof do
        do
        {
            if (bolLeerSigCar)
            {
//		previa_c = c;
                c = getc(ptrArchivoFuente);
                //cout<<"CARACTER LEIDO\t" << c << endl;
//		cout<<"CARACTER anterior\t" << previa_c << endl;
            }
            else
                bolLeerSigCar = true;
            
            intCodigoSimboloEntrada = ObtenerCodigoSimboloEntrada(c);
            //s := mov(s,c);
            intEstadoSiguiente = MT[intEstadoActual][intCodigoSimboloEntrada];
            intTipoNodo = MT[intEstadoActual][AUTOMATA_TIPO_NODO];
//	    if (c == 'P' || c == 'I')
//	    {
//		    cout << intEstadoSiguiente << " = MT [ " << intEstadoActual << "][" << intCodigoSimboloEntrada << "]" << endl;
//		    cout << intTipoNodo << endl;
//	    }
		    //c := siguientecaracter
             //Si se ha reconocido un token del lenguaje
            if ((intEstadoSiguiente == SCANNER_TOKEN_RECONOCIDO) && ((intTipoNodo == TIPONODO_FINAL) ||
                    (intTipoNodo == TIPONODO_INTERMEDIO_FINAL)))
            {   //Se ha reconocido un token
                intCodigoSimboloTerminal = MT[intEstadoActual][AUTOMATA_CODIGO_SALIDA];
                strTokenSimboloTerminal = ObtenerEtiquetaDelSimboloTerminal(intCodigoSimboloTerminal);
//                cout<<"Token reconocido:\t" << strTokenSimboloTerminal <<endl;
                //guardar token reconocido
//                cout << "ANTES DE PUSH_BACK\nintCondSImbolo\t" << intCodigoSimboloEntrada <<"\tintSig\t" << intEstadoSiguiente << "\tintActual" << intEstadoActual << endl;
                guardarToken(strTokenSimboloTerminal);
//		TokensReconocidos.push_back(strTokenSimboloTerminal);
                strTokenSimboloTerminal = "";
                intEstadoActual = 0;
                bolLeerSigCar = false;
            }
            else if (intEstadoSiguiente == SCANNER_ERROR)
            {
                //Se ha cometido un error l�xico
                cout << c << "Error Lexico" << endl;
                //break; //del do-while
            }
            else
            {
                //Sigo reconociendo dentro del autómata
                intEstadoActual = intEstadoSiguiente;

            }


        //end;
        }while (c != EOF); 
        
	
        DesplegarTokensReconocidos();

        //if s está en F then
            //return "si"
        //else return "no";
    }

    return 0;
}//scanner


void Scanner::guardarToken(string token)
{
    if(token == "ESP")
	return;
    else
	if(token == "TAB")
	    return;
	else
	    if(token == "SALTO_DE_LINEA")
		return;
	    else
		TokensReconocidos.push_back(token);
}


void Scanner::DesplegarTokensReconocidos()
{
    cout << "\n\t\tVECTOR TOKENS Reconocidos\n" <<endl;
    for(int x = 0;x< TokensReconocidos.size();x++)
    {
	cout << "TOKEN:\t" << TokensReconocidos.at(x) << endl;
    }
}

vector<string> Scanner::getVectorTokensReconocidos()
{
    return TokensReconocidos;
}