/* 
 * File:   Scanner.h
 * Author: blacke
 *
 * Created on April 11, 2013, 11:29 AM
 */

#include <iostream>
#include <string>
#include <vector>

#pragma once 

//#ifndef SCANNER_H
//#define	SCANNER_H




using namespace std;

class Scanner {
public:
    Scanner();
    Scanner(const Scanner& orig);
    virtual ~Scanner();
    int escanear(string strArchivo);
    vector<string> getVectorTokensReconocidos(); 
private:
    vector<string> TokensReconocidos;
    int ObtenerCodigoSimboloEntrada(int intASCIISimboloEntrada);
    string ObtenerEtiquetaDelSimboloTerminal(int intCodigoSimboloTerminal);
    void DesplegarTokensReconocidos();
    void LeerArchivo(string path);
    void guardarToken(string token);
};

//#endif	/* SCANNER_H */

